+++
menus = 'main'
title = 'Uses'
+++

### For drawing

*   iPad Pro (2018)
*   Apple Pencil (first generation)
*   Procreate
*   Procreate Dreams
*   Sketchbook
*   Canson Bristol paper and others
*   Winsor & Newton watercolor paper, and Arches watercolor paper for the snooty stuff
*   Multiple small watercolor kits, but mostly Cotman 12 colors
*   Hiro 111 EF Leonhardt dip pen, Talens and Winsor & Newton inks
*   Epson XL-11000+ scanner; more than 20 years old and still going strong for A3 scanning

### General computing

*   MacBook Pro, mid 2015 (daily driver)
*   iMac, late 2012 with Wacom Cintiq 13″ from… 2008, I think
*   Affinity Suite (Photo, Designer & Publisher)
*   Pixelmator
*   Libreoffice
*   IA Write
*   Aquamacs (org-mode)
*   [pngout](https://www.reinderdijkhuis.com/wordpress/2009/03/24/howto-install-pngout-on-a-mac-and-do-batch-operations/)
*   Signal
*   MAMP
*   WordPress with Toocheke or ComicEasel, depending on the project
*  Hugo
*   HomeBrew\*

\* HomeBrew deserves a special mention because it’s what I use to install a lot of small FOSS apps that I don’t want to list individually here.

### Music production

![My semi-famous travel setup](/wordpress/wp-content/uploads/2022/05/2022-travel-setup.jpeg)

*   ZOOM R-16
*   Arturia MicroFreak
*   Arturia DrumBrute
*   Arturia Spark LE (sporadically when using GarageBand, but all my music production is sadly sporadic)
*   Arturia Keystep
*   Alesis SR16 drum machine, built in 1990
*   Teenage Engineering OP-1
*   Teenage Engineering PO-12
*   Teenage Engineering PO-14
*   Teenage Engineering PO-16
*   1988 Squier Stratocaster
*   Ibanez UE 303 multi-effect
*   Zoom CDR-70 Multi-stomp
*   Moo-er booster, tube screamer and distortion pedals
*   Strymon Blue Sky reverb
*   Strymon Deco saturation pedal
*   Orange Crush 30R guitar amp
*   Ibanez Tube Screamer decommissioned after 35 years
*   Sterling by Music Man Stingray short-scale bass guitar
*   Hoffner Shorty travel bass guitar
*   Sterling by Music Man Cutlass HSS guitar
*   Seagull Merlin M4
*   VOX Pathfinder Bass 10 amplifier
*   Korg NTS-1
*   Korg MonoTron Delay
*   Korg Volca Drum
*   Korg Volca Modular
*   Audacity
*   GarageBand
*   Korg OpSix native
*   Arturia MiniFuse 2
*   Krk Rokit 6 monitor speakers 

Note: I own more than this, but these are the only things I’ve so far done any recordings with. More of that is coming when Stuff Stops Happening.

### Translation work

When I was a translator, I used a bunch of CAT programs on a HP laptop running Windows 10. The laptop, supplied by work, is fine and the VPN works well. However, I cannot with a straight face endorse any CAT program because they’re all poorly-made, bloated and difficult to use. Besides, the reason they exist is for clients to claw back financial windfalls resulting from the presence of repetitive text in the source material offered for translation. Seriously, wouldn’t you put up with manually copy-pasting repetitions if it meant you got paid full price for them? As such, all of them are harmful. When I need to translate for myself or people I care about, I use OmegaT, but no client ever asks me to use that. The system came with Total Commander, which is pretty good and I should get something like that for my other systems.

### Daily living

I use a range of stuff, some of it branded, some of it from thrift stores. The oldest pair of pants I have worn in the past year is 30 years old and has had two previous owners. I won’t bore you by listing all of it, but a number of items matter to me and are meaningfully Things That I Use:

*   Ecco Track 25 shoes; I buy the same model whenever my last pair wears out and have done so for over 20 years. I put 5000 kilometers on the last pair I bought, during two years of COVID “lockdown” I can no longer wear these due to changes in my feet, alas
*   Koga Miyata touring bike from the 1990s. I’ve mentioned this one on the blog a few times
*   Sony WH-1000 XM3 wireless noise cancelling headphones, also known as my sanity savers
*   iPod Classic, 128 GB; still my daily music driver at the home office I no longer regularly use this.
*   Denon (check model) desktop 3-in-one hi-fi system. Has a CD player, analog in AND bluetooth, so it’s the right system for my home office
*   iPhone 12 Pro; this will be my phone until 2026 if I can help it
