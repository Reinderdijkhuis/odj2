+++
menus = 'main'
title = 'Big Link List'
+++
### My stuff

[Tess Durban](https://www.reinderdijkhuis.com/tessdurban/) (English) | [Tess Durban (Danish)](https://tessdurban-dk.cfw.me/) | [Tess Durban](https://tessdurban-qu.cfw.me/) (French-Quebec)

[Rogues of Clwyd-Rhan](http://rocr.thecomicseries.com/) | [Røverne Fra Clwyd-Rhan](https://rovernefraclwydrhan.cfw.me/)  
[Abúi’s Travels](https://www.reinderdijkhuis.com/abuistravels/)

[Chronicles of the Witch Queen](https://cotwq.thecomicseries.com/) (collaboration with Geir Strøm and Daniel Østvold)  
[White House In Orbit](http://whio.thecomicseries.com/ "White House In Orbit") by Geir Strøm and Reinder Dijkhuis  
[Greyfriar’s Isle](https://www.reinderdijkhuis.com/greyfriars/) | [L’Isle Des Frères Gris](https://www.reinderdijkhuis.com/greyfriars-fr/) | [Frisk Luft Over Vadehavet](https://www.reinderdijkhuis.com/greyfriars-dk/) | [Waddenlicht](https://www.reinderdijkhuis.com/greyfriars-nl/) | [A Ilha de Greyfriar](https://www.reinderdijkhuis.com/greyfriars-pt-br/)  
[Aphantasia](https://www.reinderdijkhuis.com/aphantasia/)  
[Virtuous](https://www.reinderdijkhuis.com/virtuous/)  
[Pin Drop](https://www.reinderdijkhuis.com/pindrop/)

[The Lives of X!Gloop](https://www.reinderdijkhuis.com/xgloop/ "The Lives of X!Gloop") (excerpts)

[Support my comics on Patreon](https://www.patreon.com/Reinder)  
[My Mastodon profile](https://mastodon.art/@reinderdijkhuis#)  
[My Akkoma profile](https://pl.nudie.social/Reinderdijkhuis)  
[My Pixelfed profile](https://pixelfed.social/reinderd)  
[My DeviantArt stuff](http://reinder.deviantart.com/)  
[My Last.FM stuff](http://last.fm/user/reinderd/)  
[My Tumblr](https://www.tumblr.com/blog/reinderdijkhuis)  
[My Soundcloud](https://soundcloud.com/reinder-dijkhuis)  
[My LinkedIn profile](https://www.linkedin.com/in/reinderdijkhuis/)

### Webcomics I read or have read

[1977 The Comic](https://1977thecomic.com/)  
[Alien Circus](https://www.theduckwebcomics.com/Alien_Circus/)  
[Android Blues](https://androidblues.thecomicseries.com/)  
[The Apex Society](https://apexsociety.thecomicseries.com/)  
[Babaylan Bay](https://babaylanbay.cfw.me/)  
[Babaylan Bay Bits](https://babaylanbaybits.the-comic.org/comics/)  
[Bring Your Wings](https://bringyourwings.thecomicseries.com/)  
[Bybloemen](https://www.bybloemen.com/)  
[The Cameocomic](http://cameocomic.comicgenesis.com/)  
[Chasing the Sunset](http://www.fantasycomic.com/)  
[The Challenges of Zona](http://barbarianprincess.com/)  
[Cataclysm](https://cataclysm.thecomicseries.com/)  
[Children of the Dark Woods](https://nattonna.neocities.org/)  
[Clan of the Cats](http://www.clanofthecats.com/)  
[Cloudy With A Chance Of Fairies](https://cloudy.thecomicseries.com/)  
[Cochlea & Eustachia](https://chromefetus.thecomicseries.com/)  
[The Cosmic Beholder](https://cosmicbeholder.blogspot.com/)  
[Cowboys & Crossovers](https://cowboysandcrossovers.thecomicseries.com/)  
[Curvy](http://c.urvy.org/)  
[Dear World](https://dearworld.thecomicstrip.org/)  
[Delve](http://thisis.delvecomic.com/NewWP/)  
[Demoniac Verse](https://demoniacverse.thecomicseries.com/)  
[Diary of the Astro-Nudes](https://astronudes.thecomicseries.com/)  
[Dragon Husbands](https://dragonhusbands.thecomicseries.com/)  
[Dragons And Silk](https://dragonsandsilk.thecomicseries.com/)  
[Egress](https://egress.thecomicseries.com/)  
[Elf](https://elf-comic.thecomicseries.com/)  
[Empress](https://empress.thecomicseries.com/)  
[Evil Overlords United](http://eou.comicgenesis.com/)  
[Exiern](http://www.exiern.com/)  
[Faelings](https://www.theduckwebcomics.com/Faelings/)  
[Faeling Stories](https://www.theduckwebcomics.com/Faeling_Stories/)  
[Fiona Poppy](http://fionapoppy.thecomicseries.com/)  
[GOP Nuts](https://gopnuts.thecomicseries.com/)  
[Gorgeous Princess Creamy Beamy](http://creamybeamy.comicgenesis.com/)  
[The Halloween Cameo Caper 2012: The Crossroads of Tyrion](https://halloween2012.thecomicseries.com/)  
[The Halloween Cameo Caper 2013: Crisis In Candyland](https://halloween2013.thecomicseries.com/)  
[The Halloween Cameo Caper 2014: Fantastic Fantasy Island](https://halloween2014.thecomicseries.com/)  
[The Halloween Cameo Caper 2016: Shelterville of Oz](https://halloween2016.thecomicseries.com/)  
[The ‘Ham](https://theham.webcomic.ws/)  
[Helicopter](https://helicopter.the-comic.org/)  
[Heliothaumic](https://tropedia.fandom.com/wiki/Heliothaumic) (link goes to Fandom Wiki description)  
[Holy Cow! Christian Comics](https://web.archive.org/web/20060316081631/http://www.webcomicsnation.com/deanrankine/) (archive.org)  
[I Want You](https://iwantyou.thecomicseries.com/)  
[Kaza’s Mate, Gwenna](http://kaza-and-gwenna.thecomicseries.com/)  
[The Keep On The Borderlands](https://thekeepontheborderlands.thecomicseries.com/)  
[Knights of Samaria](https://knightsofsamaria.thecomicseries.com/)  
[Life and Death](https://www.theduckwebcomics.com/Life_and_Death/)  
[Lilly MacKenzie](https://comicvine.gamespot.com/lilly-mackenzie/4005-83096/issues-cover/) (listing of print comics for sale)  
[Lumia’s Kingdom](http://lumia.comicgenesis.com/)  
[Magellan](http://magellanverse.com/)  
[Marble Gate Dungeon](https://marblegate.webcomic.ws/)  
[Materia Enhanced](https://materia.webcomic.ws/)  
[The Meek](http://www.meekcomic.com/)  
[Mildreth Of The Night](https://mildreth.thecomicseries.com/)  
[Mindfold](https://mindfoldcomic.webcomic.ws/)  
[Monster Cravings](https://monster-cravings.thecomicseries.com/)  
[Muffin Topp](https://muffintopp.thecomicseries.com/)  
[A Murder of Two](https://www.theduckwebcomics.com/A_Murder_of_Two/)  
[The Naked Elf](http://nakedelf.comicgenesis.com/)  
[Never Mind the Gap](http://nmg.thecomicseries.com/)  
[Nortverse](https://nortverse.com/)  
[Oglaf](http://www.oglaf.com/)  
[The Pale](https://thepale.webcomic.ws/)  
[Pimpette & Associates](https://pimpette.thecomicseries.com/)  
[Pewfell](https://www.planeturf.com/pewfell2020/)  
[Questionable Content](http://questionablecontent.net/)  
[Real Life](https://reallifecomics.com/)  
[Regarding Dandelions](https://regardingdandelions.thecomicseries.com/)  
[Rental Goddess](https://rentalgoddess.thecomicseries.com/)  
[The Sagas of Seelhoë](https://thesagasofseelhoe.thecomicseries.com/)  
[Scary Go Round](http://www.scarygoround.com/)  
[Scrub Diving](https://scrubdiving.thecomicseries.com/)  
[Secret Fortress Golganooza](https://secretfortressg.thecomicseries.com/)  
[Sharing A Universe](http://sharingauniverse.comicgenesis.com/)  
[Spacegirl 8](https://spacegirl-8.thecomicseries.com/)  
[The Squirrel Machine](https://squirrelmachine.webcomic.ws/)  
[Stanton Creek](https://stantoncreek.thecomicseries.com/)  
[Storm Clouds](https://storm-clouds.thecomicseries.com/)  
[Tales from the Elfwoods](https://elfwoods.thecomicseries.com/)  
[Tangentville](https://tangentville.thecomicseries.com/)  
[Ten Earth Shattering Blows](https://tenearthshatteringblows.com/)  
[Theia Mania](https://theiamania.thecomicseries.com/)  
[Those Unknowable: Shadows Over Innsmouth](https://tsoi.thecomicseries.com/)  
[Tusk](https://thelandoftusk.thecomicseries.com/)  
[The Unclean](https://theunclean.thecomicseries.com/)  
[A View of Venus](https://www.theduckwebcomics.com/A_View_of_Venus/)  
[The Wilder Shore](https://thewildershore.the-comic.org/)  
[Wyrdhope](https://wyrdhope.thecomicseries.com/)  
[YAFGC](http://yafgc.net/)

### Related

[The Webcomic Crossover and Cameo Archive](http://crossovers.dragoneers.com/)  
[Webcomics Online](http://webcomicsoffline.com/)

### Music sites

[Alpha Alpha on YouTube](https://www.youtube.com/playlist?list=PLIhXJe-Eye9iOJqSLNVqGmVz0Frf9oCvw)  
[Angry Metal Guy](https://www.angrymetalguy.com/)  
[DGM Live](https://www.dgmlive.com/)  
[February Album Writing Month](https://fawm.org/) (only available part of the year)  
[The Highway Star](http://www.thehighwaystar.com/) (Deep Purple and family)  
[The Hooded Crow on YouTube](https://www.youtube.com/playlist?list=PLIhXJe-Eye9jbeQG09v7AslRAyKgKaogN)  
[Kate Bush News](http://www.katebushnews.com/)  
[Metal Sucks](https://www.metalsucks.net/)  
[Richard Thompson Music](http://richardthompson-music.com/)

### Writers and thinkers

[A.J. Roach](https://ajroach42.com/)  
[Alex Daily](http://blog.alexdaily.nl/) – Ey I’m Blogging Here  
[A.R. Moxon](https://armoxon.substack.com/) – The Reframe  
[Cory Doctorow](https://pluralistic.net/)  
[Emily F. Gorcenski](https://emilygorcenski.com/post/whats-new-edition-2023-11-27/)  
[Jacob Bacharach](https://jacobbacharach.com/blog/)  
[Peter Breedveld](https://www.frontaalnaakt.nl/) – Frontaal Naakt  
[Matthew Graybosch](https://starbreaker.org/) – Starbreaker  
[John Scalzi](https://whatever.scalzi.com/) – Whatever  
[Martin Wisse](http://wissewords2.cloggie.org/wissewords2/) – Wissewords

### Environment

[Extinction Rebellion Groningen](https://extinctionrebellion.nl/groep/groningen/)

### Languages

[Strong Language](https://stronglang.wordpress.com/)

### Media and arts

#### Doctor Who

[Eruditorum Press](https://www.eruditorumpress.com/)  
[GigaWho](https://gigawho.wordpress.com/)  
[Planet of the Blog](http://who.alexdaily.nl/)

### Lifestyle

#### Tech

[404 Media](https://www.404media.co/)  
[Exit Reviews](https://www.looria.com/reviews/)  
[Soatok](https://soatok.blog/b/)

#### Cycling

[Fietsers Afstappen](https://www.fietsersafstappen.nl/)

#### Naturism

[Clothesfree Forums](http://www.clothesfreeforum.com/forum.php) All about nudism, naturism and clothesfree living.  
[Les Amis Naturistes](https://les-amis-naturistes.com/forum/) (French-language forum, may be unavailable for some IP addresses)  
[NOL Hoogezand-Sappemeer](https://nolhoogezandsappemeer.nl/)
