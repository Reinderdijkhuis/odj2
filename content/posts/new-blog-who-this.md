+++
title = 'New Blog, Who This'
date = 2023-12-27T12:44:44+01:00
categories = ['metablogging']
tags = ['hugo']
+++
Oh, hi.

I had a bit of a problem over Christmas where my MacBook failed catatrophically while MAMP Pro, the app I use to build my Wordpress sites locally, was exporting pages. I recovered everything, but because the blog archive is so big, and I had to double-check the whole thing, it took forever. So to prevent losing a whole day of my life to this issue again, I'm gonna archive "Obsession Du Jour", and start anew, and also I'm going to work with something that doesn't have as many dependencies as WordPress. So I'm giving Hugo a spin using the No-style-please theme. So far, it's going reasonably well and the site is super-fast especially with this new template. Having said that, I know very little about Hugo and some things may be a little wonky for a while. 

You can find all the old posts here: [Obsession Du Jour](http://reinderdijkhuis.com/wordpress/). The new blog will be called Obsession Du Jour 2. For the time being, all the webcomics will continue to be developed in Wordpress with static exports, but eventually, I'm sure I'll learn to do them in Hugo, too.

A couple of notes, because I forget things. I installed Hugo through [Homebrew](https://brew.sh) and used the theme [nostylesplease](https://themes.gohugo.io/themes/hugo-theme-nostyleplease/), chosen for its speed and minimalism, and its visual resemblence to a screen in IAwriter, my favorite text editor. I've been modifying the theme quite a bit already, with a top menu added to posts, and tags and categories included in every post at write time. However, I don't yet know how to make them show up correctly. (At the time of writing, categories and tags show up as just words, without the links. The method described in [this tutorial](https://bwaycer.github.io/hugo_tutorial.hugo/taxonomies/displaying/) doesn't work for me. What did work was a bit of help from Cariad Eccleston who showed me [this bit of code](https://github.com/cariad/beep.blog/blob/main/themes/beep/layouts/_default/baseof.html#L126-L130) that works for both tags and categories with minor changes.
The Next and Previous links were added with the help of [this tutorial](https://gohugo.io/methods/page/prev/), which confusingly claims that the parameters do the opposite of what you'd expect. Actually, they do what you expect, if you're a blogger building a chronological archive and 'next' should go to the page that is more recent than the one you're on.
I added a shortcode for YouTube playlists based on [this Stack Overflow question
and anwer](https://stackoverflow.com/questions/72065012/how-to-embed-a-youtube-playlist-in-hugo-website), and one for Soundcloud playlists based on [a post by Mr. Cupp](https://mrcupp.com/post/2023-09-27-shortcode-soundcloud/). However, while the Soundcloud playlist shortcode works, I cannot figure out how to get a numerical playlist ID off Soundcloud, so I'm unable to use it.


The existing "Now", "Big Link List", "About" and "Uses" pages were converted from HTML to Markdown using an [online converter](https://codebeautify.org/html-to-markdown)
I am not currently deploying pages through a repository and was initially resistent to doing so when secure ftp works just as well and is something I already use. However, this means that the actual live server only lives on one machine, which is a fragile solution.

At the development stage, I'm doing most of my work in emacs in Markdown mode; however, my aim is to write individual posts in IA Writer.
