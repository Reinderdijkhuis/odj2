+++
title = 'Items of Interest, December 26, 2023 - January 2, 2024: Facts and figures'
date = 2023-12-27T13:04:55+01:00
categories = ['linklog', 'music', 'science','politics','tech']
tags = ['covid-19', 'trans', 'FAWM']
+++

[No "Social Contagion" - Gender Transition Rates Reach Equilibrium In New Study](https://www.erininthemorning.com/p/no-social-contagion-gender-transition) - Erin In The Morning. 
> The leveling off is significant because it closely mirrors another major event where another once-discouraged trait became slowly accepted by society: left-handedness. In the early 1900s, the rates of left-handedness hovered between 3-4%. Left-handedness rates then “skyrocketed” to 12% where it has leveled off ever since. This was, of course, not caused by a “massive social contagion” of left-handedness. Rather, increases in acceptance led to people feeling comfortable using their left hand. In the 1940s, anti-left-handedness researcher Abram Blau decried the “cultural influences” of left-handedness and the “progressive campaigns” of allowing the child “to be free to choose the side for himself.”

[What to Know About COVID’s Connection to Heart Problems](https://www.prevention.com/health/health-conditions/a46131357/heart-problems-after-covid/) - Meryl Davids Landau, Prevention

> Yet some of the increase was attributable to COVID itself. “The virus can directly affect the heart, such as by inflaming the cardiac muscle or the layers around it. It may also cause dysregulation of neuron fibers feeding the heart, whether via direct effect or through immune system activation,” Dr. Heidari says. Plus, long COVID, which affects some 11% of those infected, frequently includes a cardiac component, such as glitches in the autonomic nervous system that trigger palpitations.

[Golf Must Be Abolished](https://freedium.cfd/https://edithcharles.medium.com/golf-must-be-abolished-ed716f3854b6) - Edith Laurie Charles on ~~Freedium~~Medium. This warms the cockles of my heart. Golf is bad and you should feel bad for playing it.

[Half rant half story](https://www.tumblr.com/dduane/737900044438667264/half-rant-half-story-im-a-physicist-i-work-for) - fuck-customers on Tumblr.

> He was seriously asking. I've met my fair share of idiots but I was sure he wasn't genuinely seriously asking that I add AI directly to a piston system, but he was. And not even in the like "oh if we implement a way for AI to control that part" kind of way, he just vaguely thought that AI would "make it better" WHAT THE FUCK DO YOU MEANNNNN I HAD TO SPEND 20 MINUTES OF MY HARD EARNED LIFE EXPLAINING THAT NEITHER I NOR ANYONE ELSE CAN ADD AI TO A GOD DAMNED FUCKING PISTON. "CAN YOU ADD AI TO THE HYDRAULICS" NO BUT EVEN WITHOUT IT THAT METAL PIPE IS MORE INTELLIGENT THAN YOU.

I had never heard of [Maxim Februari](https://1ft.io/proxy?q=https://www.nrc.nl/nieuws/2023/12/22/maxim-februari-iedereen-zei-jij-lost-al-onze-problemen-op-a4184900) and 'writer connected with NRC Handelsblad' isn't the prize CV entry it used to be, but he seems interesting.

[The Guys Who Killed Toys R Us are Coming for Doctors](https://skepchick.org/2023/12/the-guys-who-killed-toys-r-us-are-coming-for-doctors/) - Rebecca Watson, Skepchick. It seems there's more recognition now than back in the day that Toys'R'Us didn't die of natural causes but was murdered by private equity. What Watson is describing is a little worse than that, I'd say.

I've been deeply immersed in Hugo as part of setting up this new blog. Naturally, I'm interested in using Hugo for future webcomics, too, and as it happens, I've found a basic setup for just that: [Mini Hugo Webcomic](https://github.com/AvaLovelace1/mini-hugo-webcomic). It looks pretty good and comes with a cute demo comic.

[No Time To Panic](https://terikanefield.com/no-time-to-panic/) - Teri Kanefield. Panicking helps autocrats and would-be autocrats.

I'm eagerly awaiting the start of February Album Writing Month, or [FAWM](https://fawm.org/) because that was such a creative high point of the year for me in 2023, before I got COVID and before the summer of chaos that really crimped my style. The weekly Euro FAWM chats have been a high point of each week for me as well over the past few weeks. One thing that the FAWM people have done that have helped tide me over is their re-uploads of the "4 Tracks 1 Tape" series on their official YouTube channel. They used to be on the personal channel of FAWM main man Burr Settles, but the organization's channel is a more natural home for them. Here's the latest re-upload, "[Psychedelic Circus](https://www.youtube.com/watch?v=KFjLrqiEAeY)" from 2021:

{{< youtube id="KFjLrqiEAeY" title="4 TRACKS 1 TAPE: &quot;Psychedelic Circus&quot; (Music Collab by Mail)" >}}

I hadn't followed T-Pain's work but Wikipedia says he's widely known for his extensive use of AutoTune. Here he is in an [apparently untuned live covers show](https://www.youtube.com/watch?v=91ck0vJBygo) with a great rock band, and it's amazing:
{{< youtube id="91ck0vJBygo" title="T-Pain - On Top Of The Covers" >}}

