+++
title = 'ICYMI'
date = 2023-12-31T14:51:43+01:00
draft = true
categories = ['music','metablogging','art']
tags = ['The Hooded Crow','Alpha Alpha']
+++
In the transition to the new blog, there are a few posts that may not get read as much as they otherwise might have. Here are the last few posts from the old blog:

[The Expecting To Play and The Double sessions](https://www.reinderdijkhuis.com/wordpress/2023/12/26/the-expecting-to-play-and-the-double-sessions/) - the story of how Daniel &Oslash;stvold, painter, sculptor and cartoonist came to record with the members of The Hooded Crow, with as much of the results of those sessions from the late 1990s as I'm able to share.

[Testing Webglaze](https://www.reinderdijkhuis.com/wordpress/2023/12/25/testing-webglaze/) - Three new *Tess Durban* pages uploaded with increasing levels of Glaze anti-AI protection added through Webglaze. The Glaze pixels are visible with the naked eye and the files are larger, but it's honestly not too bad.

[Items of Interest: December 19 – December 26, 2023: While we’re on the subject of fascism](https://www.reinderdijkhuis.com/wordpress/2023/12/20/items-of-interest-december-19-december-26-2023/) - This talks about fascism a lot and also brings back the semi-regular 'Elon Musk Is An Absolute Tit' feature. But also Todd McFarlane, the history of a famous Latin Christmas carol, a The Hooded Crow two-fer and more.

[Decemboobs 2023](https://www.reinderdijkhuis.com/wordpress/2023/12/18/decemboobs-2023/) - 18 new drawings highlighting titties for December. I've actually made a Glazed version of this collection to publish in places that I don't trust not to scrape them for LLM training.


