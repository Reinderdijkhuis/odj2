+++
menus = 'main'
title = 'Highlights'
+++
This blog has been around for a long time. Now that we’re on a static version with no built-in search feature, it’s time to highlight a small selection of things that I think have lasting value. This list is short; it should be, in order to be useful.

### Highlights from the old blog ###

[One year of static website building](https://www.reinderdijkhuis.com/wordpress/2023/08/30/one-year-of-static-website-building/)  
[Using webcomics plugins in WordPress with static deployment – what I’ve learned](https://www.reinderdijkhuis.com/wordpress/2023/08/31/using-webcomics-plugins-in-wordpress-with-static-deployment-what-ive-learned/)  
[Some useful instructions for installing and using PNGout](https://www.reinderdijkhuis.com/wordpress/2009/03/24/howto-install-pngout-on-a-mac-and-do-batch-operations/)  
[30 Days of Characters, 2022 edition](https://www.reinderdijkhuis.com/wordpress/2022/05/01/30-days-of-characters-2022/)  
[30 Days of Characters, 2021 edition](https://www.reinderdijkhuis.com/wordpress/2021/05/01/30-days-of-characters/)  
[Jethro Tull albums ranked and a comparison of rankings](https://www.reinderdijkhuis.com/wordpress/2022/02/08/jethro-tull-albums-ranked-and-a-comparison-of-rankings/)  
[Creative Commons Licenses Considered Harmful](https://www.reinderdijkhuis.com/wordpress/2021/11/27/items-of-interest-november-27-2021-creative-commons-licenses-considered-harmful/)  
[Never Trust a Platform Not To Sell You Out](https://www.reinderdijkhuis.com/wordpress/2021/03/18/never-trust-a-platform-not-to-sell-you-out/)

