+++
menus = 'main'
title = 'Now'
+++

### I am no longer a translator!

I have left my employer, and my translation career, after 15 years, because I'm done. My tenure there ended on January 1, 2023.

### What else is going on in my life

I’m processing a diagnosis of Palindromic Rheumatism and the realization that I’m probably going to be one of the lucky duckies for whom that syndrome morphs into ‘proper’ Rheumatoid Arthritis. I have also been diagnosed with psoriasis, and am still in recovery from COVID-19 which I caught in April 2023.  
My oldest stepson is now getting back to work after a major health crisis. Trying to get my youngest stepson back from the US because he’s done with living there. Our not-a-stepson who is like our own kid to us also had a major health crisis recently and is now recovering from that.  
I have two dogs, Séamus and Dexter. Séamus is a Shih Tzu mix and an eternal puppy. Dexter is a dachshund mix, a little older and completely blind. Both are very high-energy dogs so I walk them a lot.

We now grow and preserve more of our own food. We’re a long way from self-sufficiency but learning a lot.  
We have bought a property in Portugal, which will be our winter hideout and where we will also grow a lot of our own food. Traveling there again in January, 2023.

### What I’m working on

  
This website, in its fifth incarnation. You’re looking at it now: it’s built as a collection of static websites exported from a collection of WordPress installations that live on my laptop.  
Webcomic [Greyfriar’s Isle](https://www.reinderdijkhuis.com/greyfriars/), Danish version of webcomic [Rogues of Clwyd-Rhan](https://rovernefraclwydrhan.cfw.me/) (both NSFW). Drawing the third chapter of [Tess Durban](https://cultishmanners.cfw.me/). [Looking forward to FAAWM](https://fiftyninety.fawm.org/@reinderd).  
Translator and editor at [Global textware](http://globaltextware.nl/), part-time since May 2020, ending on January 1, 2023. Looking for work in the communications sector as of January 1, 2024.
Practicing guitar and bass almost daily for up to two hours at a time, relearning old songs and writing new ones.

### What I’m thinking about

Synthesizers, computers, recording, keeping aging computers alive and usable so I don’t have to spend on new ones and cause e-waste. How to manage my time and energy with multiple conditions that affect executive function. The era of collapse we’re all sleepwalking into.
