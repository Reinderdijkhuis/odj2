+++
menus = 'main'
title = 'About'
+++
Reinder Dijkhuis is a cartoonist, sometime musician, former translator and creator of [Rogues of Clwyd-Rhan](https://rocr.cfw.me/), [Tess Durban](https://reinderdijkhuis.com/tessdurban/) and [Greyfriar’s Isle](https://reinderdijkhuis.com/greyfriars/). He is married with sort-of three adult sons and two dogs, and currently resides.

This blog is about anything that strikes his fancy. Sometimes there are guest posters. Sometimes they are even correctly identified and credited. Pages are static now, so there is no longer any comments or search feature. However, there is a link list and a short Highlights page.

[My DeviantArt](https://reinder.deviantart.com/)

[My Mastodon/fediverse profile](https://mastodon.art/@reinderdijkhuis)

[My LinkedIn profile](https://www.linkedin.com/in/reinderdijkhuis/)

[My Last.FM](http://last.fm/user/reinderd)

