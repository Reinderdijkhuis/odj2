+++
menus = 'main'
title = 'Then'
+++

### Stuff I've done in the past ###

I've been in several bands, though that wound down in the late 1990s, and the last band I was in was more of an excuse to get drunk with my friends and muck about with 1980s covers. Before that, though, some of the bands I was in did perform and record, including some sessions in a Real Recording Studio[tm].

#### The Hooded Crow ####
The Hooded Crow was a very productive indie rock band in Groningen, the Netherlands. We recorded many 4-track demos. I cannot guarantee that I'm personally on every track in the tracklists below, because we never tracked live; it was always done in bedrooms with overdubs, which means that the presence of more than one guitarist was never strictly required. We performed live about a dozen times per year as I recall.

First demo (1994):
{{<ytplaylist id = "PLIhXJe-Eye9jXU1hrT9OmNes0iT6Z4A15">}}

Second demo (1994):
{{<ytplaylist id = "PLIhXJe-Eye9jrN2TwM_y_D-SwUiBKnnTl">}}

Metslawier sessions (1995):
{{<ytplaylist id = "PLIhXJe-Eye9j1BL3Eejv7ITZcW1KFXyBz">}}


#### Alpha Alpha ####
Live at Rooie Oortjes Festival 1997:
{{<ytplaylist id = "PLIhXJe-Eye9iOJqSLNVqGmVz0Frf9oCvw">}}
(To do: find out how to extract playlist IDs from Soundcloud, so I can use the [Soundcloud shortcode](https://mrcupp.com/post/2023-09-27-shortcode-soundcloud/) that I got from Mr. Cupp)

I have put webcomics online almost since there was a web, starting in 1994. However, none of those early archives survive, which is good, because I had no idea what I was doing at the time. 

I have lived in Groningen, Hoogezand, Leiden and Manchester (Tennessee).


